import React  from 'react'
import './App.css'

/**
 * React version before 16.8
 */
class App extends React.Component<{}, {}> {
  constructor(props: {}) {
    super(props)
  }


  render(): JSX.Element {
    return (
      <>
      Thing!! 🐎
    </>
    )
  }
}

export default App

/**
 * React version 16.8 and later
 */
// const App: React.FC = () => {

//   return (
//     <>
//       Thing!! 🐎
//     </>
//   )
// }

// export default App
