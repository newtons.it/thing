/**
 * Write in the section below (which is not commented) when
 * writing a node application without React
 */

export const foo = () => console.log("Thing")
foo()

/**
 * Uncomment this section when using React
 * and comment the section above.

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
*/